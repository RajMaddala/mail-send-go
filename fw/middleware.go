package fw

import (
	"log"

	"github.com/labstack/echo"
)

type (
	// BasicAuthConfig defines the config for BasicAuth middleware.
	BasicAuthConfig struct {

		// Validator is a function to validate BasicAuth credentials.
		// Required.
		Validator BasicAuthValidator

		// Realm is a string to define realm attribute of BasicAuth.
		// Default value "Restricted".
		Realm string
	}

	// BasicAuthValidator defines a function to validate BasicAuth credentials.
	BasicAuthValidator func(string, string, echo.Context) (bool, error)
)

// Logger is to log the request .
func Logger() echo.MiddlewareFunc {

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {

			req := c.Request()

			//Get the auth header.
			authHeader := req.Header.Get(echo.HeaderAuthorization)
			log.Println(authHeader)
			return next(c)
		}
	}
}
