package main

import (
	"log"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/mail-send-go/mail/model"
	"gitlab.com/mail-send-go/mail/service"
)

func sendMail(c echo.Context) error {
	mail := &model.Email{}
	var err error
	if err = c.Bind(mail); err != nil {
		log.Printf("Error while binding:%v", err)
	}
	log.Println(mail)
	if err = service.SendMail(*mail); err == nil {
		return c.String(http.StatusOK, "Successful!")
	}

	return err
}
