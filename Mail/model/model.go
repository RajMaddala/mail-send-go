package model

//Person is either sender or a recipient of an Email
type Person struct {
	Name    string
	Address string
}

//Email abstracts an email
type Email struct {
	From       Person `json:"From" `
	To         Person `json:"To" `
	Cc         Person `json:"Cc" `
	Bcc        Person `json:"Bcc"`
	Subject    string `json:"Subject" `
	Body       string `json:"Body" `
	Attachment string `json:"Attachment" `
}


