package service

import (
	"log"
	"net/textproto"

	mailjet "github.com/mailjet/mailjet-apiv3-go"
	"gitlab.com/mail-send-go/mail/model"
)

//SendMail sends the mail
func SendMail(m model.Email) error {
	mailjetClient := mailjet.NewMailjetClient("7f410159725d1784c882e4524cfda32c", "401f57e8eb19258913dbe90610483e66")
	header := make(textproto.MIMEHeader)
	header.Add("From", m.From.Address)
	header.Add("To", m.To.Address)
	header.Add("Cc", m.Cc.Address)
	header.Add("Bcc", m.Bcc.Address)
	header.Add("Subject", m.Subject)
	content := []byte(m.Body)
	info := &mailjet.InfoSMTP{
		From:       m.From.Address,
		Recipients: header["To"],
		Header:     header,
		Content:    content,
	}
	if err := mailjetClient.SendMailSMTP(info); err != nil {
		log.Println("Error while sedning mail :", err)
		return err
	}
	log.Println("mail send Successfully!!")
	return nil
}
