package main

import (
	"bufio"
	"flag"
	"log"
	"os"
	"strings"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var baseURI = "mail/api"
var e *echo.Echo

type mailConfig struct {
	Publickey  string `envconfig:"Mail_Public_key"`
	Privatekey string `envconfig:"Mail_private_key"`
}

var mc mailConfig

func main() {

	var configFile string
	flag.StringVar(&configFile, "configFile", "", "If configuring externally, pass as {microservice} -configFile=/path/to/config/env")
	flag.Parse()

	if configFile != "" {
		//External configuration set. Load the configuration.
		log.Printf("Loading environment from configuration file [%s]", configFile)
		if err := loadEnvFromConfigFile(configFile); err != nil {
			log.Printf("Failed loading environment. Error is [%s]", err.Error())
			//os.Exit(1)
			return
		}
		var mc = new(mailConfig)
		mc.Privatekey = os.Getenv("Mail_private_key")
		mc.Publickey = os.Getenv("Mail_Public_key")
		log.Printf("envi[%+v]\n", mc)

	}
	e = echo.New()
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: `[${time_rfc3339}] ${status} ${method} ${host} ${path} ${latency_human}` + "\n",
	}))
	e.Use(middleware.BasicAuth(func(username, pwd string, c echo.Context) (bool, error) {
		if username == "raj" && pwd == "123" {
			return true, nil
		}
		return false, nil
	}))
	routes()

	e.Logger.Fatal(e.Start(":8143"))

}

//loadEnvFromConfigFile loads configuration from the file passed and fails on any errors encountered.
func loadEnvFromConfigFile(filepath string) error {

	const (
		CommentPrefix     = "#"
		KeyValueSeparator = "="
	)

	file, err := os.Open(filepath)
	if err != nil {
		log.Printf("Configuration file [%s] cannot be opened. Error is [%s]", filepath, err.Error())
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var keyval string
	for scanner.Scan() {
		keyval = scanner.Text()
		keyval = strings.TrimSpace(keyval)
		if keyval != "" && !strings.HasPrefix(keyval, CommentPrefix) {
			tokens := strings.SplitN(keyval, KeyValueSeparator, 2)
			log.Printf("Setting environment from file : [%s]=[%s]\n", tokens[0], tokens[1])
			os.Setenv(strings.TrimSpace(tokens[0]), strings.TrimSpace(tokens[1]))
		}
	}

	if err := scanner.Err(); err != nil {
		log.Printf("Configuration file [%s] cannot be read. Error is [%s]", filepath, err.Error())
		return err
	}

	return nil
}
